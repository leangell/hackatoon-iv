from django.shortcuts import render
from rest_framework import viewsets, permissions
from leoclinic.models import *
from leoclinic.serializers import *

# Create your views here.

class MedicoViewset(viewsets.ModelViewSet):
    queryset = Medico.objects.all()
    serializer_class = MedicoSerializer
    #permission_classes = [permissions.IsAuthenticated]

class EspecialidadViewset(viewsets.ModelViewSet):
    queryset = Especialidad.objects.all()
    serializer_class = EspecialidadSerializer
    #permission_classes = [permissions.IsAuthenticated]

class MedicosEspecialidadViewset(viewsets.ModelViewSet):
    queryset = MedicosEspecialidad.objects.all()
    serializer_class = MedicosEspecialidadSerializer
    #permission_classes = [permissions.IsAuthenticated]

class HorarioViewset(viewsets.ModelViewSet):
    queryset = Horario.objects.all()
    serializer_class = HorarioSerializer
    #permission_classes = [permissions.IsAuthenticated]

class PacienteViewset(viewsets.ModelViewSet):
    queryset = Paciente.objects.all()
    serializer_class = PacienteSerializer
    #permission_classes = [permissions.IsAuthenticated]

class CitaViewset(viewsets.ModelViewSet):
    queryset = Cita.objects.all()
    serializer_class = CitaSerializer
    #permission_classes = [permissions.IsAuthenticated]
