from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class CommonInfo(models.Model):
    fecha_registro = models.DateField(auto_now_add=True)
    fecha_modificacion = models.DateField(auto_now_add = True)
    usuarioregistro= models.ForeignKey(User, related_name="usuarios_registro", on_delete=models.CASCADE)
    usuariomodificacion= models.ForeignKey(User, related_name="usuarios_modificado", on_delete=models.CASCADE)
    activo= models.BooleanField(default=True)

class Meta:
    abstract = True

class Especialidad(CommonInfo):
    nombre = models.CharField(max_length=200)
    descripcion = models.CharField(max_length=200)

    def __str__(self):
        return self.nombre

class Medico(CommonInfo):
    nombre = models.CharField(max_length=50)
    apellidos = models.CharField(max_length=100)
    dni = models.IntegerField()
    direction = models.CharField(max_length=255)
    correo = models.EmailField()
    telefono = models.CharField(max_length=20)
    sexo = models.CharField(max_length=1)
    numcolegiatura = models.IntegerField()
    fecha_nacimiento = models.DateField()

    def __str__(self):
        return  self.apellidos

class MedicosEspecialidad(CommonInfo):
    medico_id = models.ForeignKey(Medico, on_delete=models.CASCADE)
    especialidad_id = models.ForeignKey(Especialidad, on_delete=models.CASCADE)

class Horario(CommonInfo):
    medico_id = models.ForeignKey(Medico, on_delete=models.CASCADE)
    fecha_atencion = models.DateField()
    inicio_atencion= models.TimeField()
    final_atencion = models.TimeField()

    def __unicode__(self):
        return self.fecha_atencion

class Paciente(CommonInfo):
    nombre = models.CharField(max_length=50)
    apellidos = models.CharField(max_length=100)
    dni = models.IntegerField()
    direction = models.CharField(max_length=255)
    correo = models.EmailField()
    telefono = models.CharField(max_length=20)
    sexo = models.CharField(max_length=1)
    fecha_nacimiento = models.DateField()

class Estado(models.Model):
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return  self.nombre



class Cita(CommonInfo):
    medico_id = models.ForeignKey(Medico, on_delete=models.CASCADE)
    paciente_id = models.ForeignKey(Paciente, on_delete=models.CASCADE)
    fecha_atencion = models.DateField()
    inicio_atencion = models.TimeField()
    final_atencion = models.TimeField()
    estado = models.ForeignKey(Estado, on_delete=models.CASCADE)