from django.contrib import admin

from leoclinic.models import *

# Register your models here.
@admin.register(Medico)
class MedicoAdmin(admin.ModelAdmin):
    list_display = ('nombre','apellidos','dni','direction','correo','telefono','sexo','numcolegiatura','fecha_nacimiento')

@admin.register(Especialidad)
class EspecialidadAdmin(admin.ModelAdmin):
    list_display = ('nombre','descripcion')

@admin.register(MedicosEspecialidad)
class MedicosEspecialidadAdmin(admin.ModelAdmin):
    list_display = ('medico_id','especialidad')

@admin.register(Horario)
class HorarioAdmin(admin.ModelAdmin):
    list_display = ('medico_id','fecha_atencion', 'inicio_atencion','final_atencion')

@admin.register(Paciente)
class PacienteAdmin(admin.ModelAdmin):
    list_display = ('nombre','apellidos','dni','direction','correo','telefono','sexo','fecha_nacimiento')

@admin.register(Estado)
class Estado(admin.ModelAdmin):
    list_display = ('nombre',)

@admin.register(Cita)
class CitaAdmin(admin.ModelAdmin):
    list_display = ('medico_id','paciente','fecha_atencion','inicio_atencion', 'final_atencion','estado')