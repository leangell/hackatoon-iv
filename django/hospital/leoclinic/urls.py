from django.urls import path,include
from leoclinic.views import *
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'medico', MedicoViewset)
router.register(r'especialidad', EspecialidadViewset)
router.register(r'medicoespecialidad', MedicosEspecialidadViewset)
router.register(r'horario', HorarioViewset)
router.register(r'paciente', PacienteViewset)
router.register(r'cita', CitaViewset)

urlpatterns = [
    path('', include(router.urls)), 
]