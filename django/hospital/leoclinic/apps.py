from django.apps import AppConfig


class LeoclinicConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'leoclinic'
