import './index.css';
import "bootstrap/dist/css/bootstrap.min.css";

import { BrowserRouter, Route, Routes } from 'react-router-dom';

import EspecialidadForm from './components/hospital/EspecialidadForm';
import EspecialidadesList from './components/hospital/EspecialidadesList';
import Home from './components/hospital/Home'
import HorarioForm from './components/hospital/horario/HorarioForm';
import HorarioList from './components/hospital/horario/HorarioList'
import MedicoForm from './components/hospital/medico/MedicoForm';
import MedicosList from './components/hospital/medico/MedicoList';
import Navbar from './components/Navbar/Navbar';
import PacienteForm from './components/hospital/paciente/PacienteForm'
import PacienteList from './components/hospital/paciente/PacienteList'
import React from 'react'
import ReactDOM from 'react-dom/client';

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <BrowserRouter>
      <Navbar />
      <Routes>
        <Route exact="true" path="/" element={<Home />} />
        <Route exact="true" path="/lisespecialidades" element={<EspecialidadesList />} />
        <Route path="/especialidadesForm" element={<EspecialidadForm />} />
        <Route path="/updateEspecialidad/:id" element={<EspecialidadForm />} />
        <Route path="/listmedicos" element={<MedicosList />} />
        <Route path="/medicoForm" element={<MedicoForm />} />
        <Route path="/updatemedico/:id" element={<MedicoForm />} />
        <Route path="/listpaciente" element={<PacienteList />} />
        <Route path="/pacienteForm" element={<PacienteForm />} />
        <Route path="/updatepaciente/:id" element={<PacienteForm />} />
        <Route path="/listhorario" element={<HorarioList />} />
        <Route path="/horarioForm" element={<HorarioForm />} />
        <Route path="/updatehorario/:id" element={<HorarioForm />} />
        <Route path="/home" element={<Home />} />
      </Routes>
    </BrowserRouter>
  </React.StrictMode>
);
