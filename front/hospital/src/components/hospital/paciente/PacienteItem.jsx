import React from "react";
import { deletePaciente } from "./PacienteServer";
//import { deleteSpecialidades } from "./EspecialidadesServer";
import { useNavigate } from "react-router-dom";
const PacienteItem = ({ paciente }) => {
  const navigate = useNavigate();
  const handleDelete = (pacienteId) => {
    deletePaciente(pacienteId);
    navigate("/listpaciente", { replace: true });
  };

  return (
    <tr>
      <th scope="row">{paciente.id}</th>
      <td>{paciente.nombre}</td>
      <td>{paciente.apellidos}</td>
      <td>{paciente.sexo}</td>
      <td>{paciente.dni}</td>
      <td>{paciente.direction}</td>
      <td>{paciente.telefono}</td>
      <td>
        <button
          onClick={() => navigate(`/updatepaciente/${paciente.id}`)}
          className="btn btn-primary mx-2"
        >
          Update
        </button>
      </td>
      <td>
        <form>
          <button
            type="submit"
            onClick={() => paciente.id && handleDelete(paciente.id)}
            className="btn btn-danger mx-2"
          >
            Delete
          </button>
        </form>
      </td>
    </tr>
  );
};

export default PacienteItem;
