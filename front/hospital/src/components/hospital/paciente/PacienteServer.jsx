import axios from "axios";
const API_URL = "http://127.0.0.1:8000/paciente/";

export const listPacientes = async () => {
  return await fetch(API_URL);
};

export const getePaciente = async (id) => {
  return await fetch(`${API_URL}${id}`);
};

export const registerPaciente = async (file) => {
  return await axios.post(API_URL, file, {
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export const updatePaciente = async (id, file) => {
  return await axios.put(`${API_URL}${id}/`, file, {
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export const deletePaciente = async (id) => {
  return await axios.delete(`${API_URL}${id}`);
};
