import * as PacienteServer from "./PacienteServer";

import React, { useEffect, useState } from "react";

import { Link } from "react-router-dom";
import PacienteItem from "./PacienteItem";

const PacienteList = () => {
  const [pacientes, setPacientes] = useState([]);
  const listPacientes = async () => {
    try {
      const res = await PacienteServer.listPacientes(pacientes);
      const data = await res.json();
      console.log(data);
      setPacientes(data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    listPacientes();
  }, []);
  return (
    <div className="container my-5">
      <div>
        <table className="table table-dark table-striped">
          <thead>
            <tr>
              <th scope="col">Id</th>
              <th scope="col">Nombre</th>
              <th scope="col">Apellidos</th>
              <th scope="col">Sexo</th>
              <th scope="col">Dni</th>
              <th scope="col">Direccion</th>
              <th scope="col">Telefono</th>
              <th scope="col">Update</th>
              <th scope="col">Delete</th>
            </tr>
          </thead>
          <tbody>
            {pacientes.map((paciente) => {
              return <PacienteItem key={paciente.id} paciente={paciente} />;
            })}
          </tbody>
        </table>
        <Link className="btn btn-primary" to="/pacienteForm">
          Agregar paciente
        </Link>
      </div>
    </div>
  );
};
export default PacienteList;
