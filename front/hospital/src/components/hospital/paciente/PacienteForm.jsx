import * as PacienteServer from "./PacienteServer";

import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

import { registerPaciente } from "./PacienteServer";

const PacienteForm = () => {
  let navigate = useNavigate();
  const params = useParams();
  console.log(params);
  const initialState = {
    activo: true,
    nombre: "",
    apellidos: "",
    dni: 0,
    direction: "",
    correo: "",
    telefono: "",
    sexo: "",
    fecha_nacimiento: "",
    usuarioregistro: 1,
    usuariomodificacion: 1,
  };

  function handleoOnchage(e) {
    const { name, value } = e.target;
    setPaciente({ ...paciente, [name]: value });
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    try {
      if (!params.id) {
        registerPaciente(paciente)
          .then((rest) => {
            console.log(rest);
          })
          .catch((err) => {
            console.log(err);
          });
      } else {
        PacienteServer.updatePaciente(params.id, paciente);
      }

      navigate("/listpaciente", { replace: true });
    } catch (error) {
      console.log(error);
    }
  };

  const getPaciente = async (medicoId) => {
    try {
      const result = await PacienteServer.getePaciente(medicoId);
      const data = await result.json();
      const {
        nombre,
        apellidos,
        dni,
        direction,
        correo,
        telefono,
        sexo,
        fecha_nacimiento,
        usuarioregistro,
        usuariomodificacion,
      } = data;
      setPaciente({
        nombre,
        apellidos,
        dni,
        direction,
        correo,
        telefono,
        sexo,
        fecha_nacimiento,
        usuarioregistro,
        usuariomodificacion,
      });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (params.id) {
      getPaciente(params.id);
    }
  }, []);

  const [paciente, setPaciente] = useState(initialState);
  return (
    <div className="container">
      <h1 className="mb-4 text-center mt-3">Formulario Paciente</h1>
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label htmlFor="exampleInputEmail1" className="form-label">
            Nombre
          </label>
          <input
            type="text"
            className="form-control"
            name="nombre"
            value={paciente.nombre}
            onChange={handleoOnchage}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="exampleInputEmail1" className="form-label">
            Apellidos
          </label>
          <input
            type="text"
            className="form-control"
            name="apellidos"
            value={paciente.apellidos}
            onChange={handleoOnchage}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="exampleInputPassword1" className="form-label">
            Dni
          </label>
          <input
            type="number"
            className="form-control"
            name="dni"
            value={paciente.dni}
            onChange={handleoOnchage}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="exampleInputPassword1" className="form-label">
            Direction
          </label>
          <input
            type="text"
            className="form-control"
            name="direction"
            value={paciente.direction}
            onChange={handleoOnchage}
          />
        </div>

        <div className="mb-3">
          <label htmlFor="exampleInputPassword1" className="form-label">
            Email
          </label>
          <input
            type="email"
            className="form-control"
            name="correo"
            value={paciente.correo}
            onChange={handleoOnchage}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="exampleInputPassword1" className="form-label">
            Telefono
          </label>
          <input
            type="text"
            className="form-control"
            name="telefono"
            value={paciente.telefono}
            onChange={handleoOnchage}
          />
        </div>

        <div className="mb-3">
          <label htmlFor="exampleInputPassword1" className="form-label">
            Sexo
          </label>
          <input
            type="text"
            className="form-control"
            name="sexo"
            value={paciente.sexo}
            onChange={handleoOnchage}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="exampleInputPassword1" className="form-label">
            Fecha Nacimiento
          </label>
          <input
            type="date"
            className="form-control"
            name="fecha_nacimiento"
            value={paciente.fecha_nacimiento}
            onChange={handleoOnchage}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="exampleInputPassword1" className="form-label">
            Usuario Registro
          </label>
          <input
            type="number"
            className="form-control"
            name="usuarioregistro"
            value={paciente.usuarioregistro}
            onChange={handleoOnchage}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="exampleInputPassword1" className="form-label">
            Usuario Modificacion
          </label>
          <input
            type="number"
            className="form-control"
            name="usuariomodificacion"
            value={paciente.usuariomodificacion}
            onChange={handleoOnchage}
          />
        </div>
        <div>
          {params.id ? (
            <button type="submit" className="btn btn-dark">
              Editar
            </button>
          ) : (
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          )}
        </div>
      </form>
    </div>
  );
};

export default PacienteForm;
