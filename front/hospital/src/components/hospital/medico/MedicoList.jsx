import * as MedicoServer from './MedicoServer'

import React, { useEffect, useState } from "react";

import { Link } from "react-router-dom";
import MedicoItem from './MedicoItem';

const MedicosList = () => {
  const [medicos, setMedicos] = useState([]);
  const listMedicos = async()=> {
    
    try {
        const res = await MedicoServer.listMedicos(medicos);
        const data = await res.json();
        console.log(data)
        setMedicos(data);
    } catch (error) {
        console.log(error)
    }
  }
  
  useEffect(() => {
    listMedicos();
  }, []);
  return (
    <div className="container my-5">
      <div>
        <table className="table table-dark table-striped">
          <thead>
            <tr>
              <th scope="col">Id</th>
              <th scope="col">Nombre</th>
              <th scope="col">Apellidos</th>
              <th scope="col">Sexo</th>
              <th scope="col">Dni</th>
              <th scope="col">Direccion</th>
              <th scope="col">Telefono</th>
              <th scope="col">Update</th>
              <th scope="col">Delete</th>
            </tr>
          </thead>
          <tbody>
            {medicos.map((medico) => {
              return <MedicoItem key={medico.id} medico={medico} />;
            })}
          </tbody>
        </table>
        <Link className= "btn btn-primary" to="/medicoForm"> Agregar Medico</Link>
      </div>
    </div>
  );
  
};
export default MedicosList;
