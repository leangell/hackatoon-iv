import React from "react";
import {deleteMedico} from "./MedicoServer"
//import { deleteSpecialidades } from "./EspecialidadesServer";
import { useNavigate } from "react-router-dom";
const MedicoItem = ({medico}) => {
    const navigate = useNavigate();
     const handleDelete = (medicoId) => {
       deleteMedico(medicoId);
       navigate("/listmedicos", { replace: true });
    };

    return (
      <tr>
        <th scope="row">{medico.id}</th>
        <td>{medico.nombre}</td>
        <td>{medico.apellidos}</td>
        <td>{medico.sexo}</td>
        <td>{medico.dni}</td>
        <td>{medico.direction}</td>
        <td>{medico.telefono}</td>
        <td>
          <button
            onClick={() => navigate(`/updateMedico/${medico.id}`)}
            className="btn btn-primary mx-2"
          >
            Update
          </button>
        </td>
        <td>
          <form>
            <button
              type="submit"
              onClick={() => medico.id && handleDelete(medico.id)}
              className="btn btn-danger mx-2"
            >
              Delete
            </button>
          </form>
        </td>
      </tr>
    );
}

export default MedicoItem;