import axios from "axios";
const API_URL = "http://127.0.0.1:8000/medico/";

export const listMedicos = async ()=> {
    return await fetch(API_URL);
};

export const geteMedico = async (id) => {
  return await fetch(`${API_URL}${id}`);
};

export const registerMedico = async (medico) => {
  return await axios.post(API_URL, medico, {
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export const updateMedico = async (id, medico) => {
  return await axios.put(`${API_URL}${id}/`, medico, {
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export const deleteMedico = async (id) => {
  return await axios.delete(`${API_URL}${id}`);
};