import * as MedicoServer from "./MedicoServer";

import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

import { registerMedico } from "./MedicoServer";

const MedicoForm = () => {
  let navigate = useNavigate();
  const params = useParams();
  console.log(params);
  const initialState = {
    activo: true,
    nombre: "",
    apellidos: "",
    dni: 0,
    direction: "",
    correo: "",
    telefono: "",
    sexo: "",
    numcolegiatura: 0,
    fecha_nacimiento: "",
    usuarioregistro: 1,
    usuariomodificacion: 1,
  };

  function handleoOnchage(e) {
    const { name, value } = e.target;
    setMedico({ ...medico, [name]: value });
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    try {
      if (!params.id) {
        registerMedico(medico)
          .then((rest) => {
            console.log(rest);
          })
          .catch((err) => {
            console.log(err);
          });
      } else {
        MedicoServer.updateMedico(params.id, medico);
      }

      navigate("/listmedicos", { replace: true });
    } catch (error) {
      console.log(error);
    }
  };

  const getMedico = async (medicoId) => {
    try {
      const result = await MedicoServer.geteMedico(
        medicoId
      );
      const data = await result.json();
      const { 
        nombre,
        apellidos,
        dni,
        direction,
        correo,
        telefono,
        sexo,
        numcolegiatura,
        fecha_nacimiento,
        usuarioregistro,
        usuariomodificacion} =data;
      setMedico({
        nombre,
        apellidos,
        dni,
        direction,
        correo,
        telefono,
        sexo,
        numcolegiatura,
        fecha_nacimiento,
        usuarioregistro,
        usuariomodificacion,
      });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (params.id) {
      getMedico(params.id);
    }
  }, []);

  const [medico, setMedico] = useState(initialState);
  return (
    <div className="container text-bg-dark ">
      <h1 className="mb-4 text-center mt-3">Formulario Medico</h1>
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label htmlFor="exampleInputEmail1" className="form-label">
            Nombre
          </label>
          <input
            type="text"
            className="form-control"
            name="nombre"
            value={medico.nombre}
            onChange={handleoOnchage}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="exampleInputEmail1" className="form-label">
            Apellidos
          </label>
          <input
            type="text"
            className="form-control"
            name="apellidos"
            value={medico.apellidos}
            onChange={handleoOnchage}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="exampleInputPassword1" className="form-label">
            Dni
          </label>
          <input
            type="number"
            className="form-control"
            name="dni"
            value={medico.dni}
            onChange={handleoOnchage}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="exampleInputPassword1" className="form-label">
            Direction
          </label>
          <input
            type="text"
            className="form-control"
            name="direction"
            value={medico.direction}
            onChange={handleoOnchage}
          />
        </div>

        <div className="mb-3">
          <label htmlFor="exampleInputPassword1" className="form-label">
            Email
          </label>
          <input
            type="email"
            className="form-control"
            name="correo"
            value={medico.correo}
            onChange={handleoOnchage}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="exampleInputPassword1" className="form-label">
            Telefono
          </label>
          <input
            type="text"
            className="form-control"
            name="telefono"
            value={medico.telefono}
            onChange={handleoOnchage}
          />
        </div>

        <div className="mb-3">
          <label htmlFor="exampleInputPassword1" className="form-label">
            Sexo
          </label>
          <input
            type="text"
            className="form-control"
            name="sexo"
            value={medico.sexo}
            onChange={handleoOnchage}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="exampleInputPassword1" className="form-label">
            Numero colegiatura
          </label>
          <input
            type="number"
            className="form-control"
            name="numcolegiatura"
            value={medico.numcolegiatura}
            onChange={handleoOnchage}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="exampleInputPassword1" className="form-label">
            Fecha Nacimiento
          </label>
          <input
            type="date"
            className="form-control"
            name="fecha_nacimiento"
            value={medico.fecha_nacimiento}
            onChange={handleoOnchage}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="exampleInputPassword1" className="form-label">
            Usuario Registro
          </label>
          <input
            type="number"
            className="form-control"
            name="usuarioregistro"
            value={medico.usuarioregistro}
            onChange={handleoOnchage}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="exampleInputPassword1" className="form-label">
            Usuario Modificacion
          </label>
          <input
            type="number"
            className="form-control"
            name="usuariomodificacion"
            value={medico.usuariomodificacion}
            onChange={handleoOnchage}
          />
        </div>
        <div>
          {params.id ? (
            <button type="submit" className="btn btn-dark">
              Editar
            </button>
          ) : (
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          )}
        </div>
      </form>
    </div>
  );
};

export default MedicoForm;
