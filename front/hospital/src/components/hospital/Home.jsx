const Home=()=>{
    return (
      <div className="card bg-dark text-white">
        <img
          src="https://www.elhospital.com/documenta/imagenes/134597/Ranking-Hospitales-2018-G.jpg"
          className="card-img"
          alt="..."
        />
        <div className="card-img-overlay">
          <h4 className="card-title text-primary">Leo Clinic</h4>
          <p className="card-text text-primary">
            Lugar donde lo primero es Dios y usted es lo mas importante .
          </p>
        </div>
      </div>
    );
}
export default Home;