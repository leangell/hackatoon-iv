import * as EspecialidadesServer from './EspecialidadesServer';

import React, { useEffect, useState } from 'react';

import EspecialidadItem from './EspecialidadItem';
import { Link } from "react-router-dom";

const EspecialidadesList =  ()=>{
    const [especialidades,setEspecialidades] = useState([])
    
    const listEspecialidades = async()=>{
        try{
            const res = await EspecialidadesServer.listSpecialidades();
            const data = await res.json()
            console.log(data)
            setEspecialidades(data)
        }catch(e){
            console.log(e);
        }
    }
    useEffect(()=>{
        listEspecialidades()
    },[])
    return (
      <div className="container my-4">
        <div className="row">
          {especialidades.map((especialidad) => (
            <EspecialidadItem
              key={especialidad.id}
              especialidad={especialidad}
            />
          ))}
        </div>
        <Link className="btn btn-primary" to="/especialidadesForm">
          Agregar especialidades
        </Link>
      </div>
    );
};

export default EspecialidadesList;
