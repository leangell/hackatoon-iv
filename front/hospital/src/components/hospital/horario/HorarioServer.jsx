import axios from "axios";
const API_URL = "http://127.0.0.1:8000/horario/";

export const listHorarios = async () => {
  return await fetch(API_URL);
};

export const geteHorario = async (id) => {
  return await fetch(`${API_URL}${id}`);
};

export const registerHorario = async (horario) => {
  return await axios.post(API_URL, horario, {
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export const updateHorario = async (id, horario) => {
    console.log(horario)
  return await axios.put(`${API_URL}${id}/`, horario, {
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export const deleteHorario = async (id) => {
  return await axios.delete(`${API_URL}${id}`);
};
