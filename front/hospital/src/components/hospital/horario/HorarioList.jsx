import * as HorarioServer from "./HorarioServer";

import React, { useEffect, useState } from "react";

import HorarioItem from "./HorarioItem";
import { Link } from "react-router-dom";

const HorarioList = () => {
  const [horarios, setHorarios] = useState([]);
  const listEspecialidades = async () => {
    try {
      const res = await HorarioServer.listHorarios();
      const data = await res.json();
      setHorarios(data);
    } catch (e) {
      console.log(e);
    }
  };
  useEffect(() => {
    listEspecialidades();
  }, []);
  return (
    <div className="container my-4">
      <div className="row">
        {horarios.map((horario) => (
          <HorarioItem key={horario.id} horario={horario} />
        ))}
      </div>
      <Link className="btn btn-primary" to="/horarioForm">
        Agregar horario
      </Link>
    </div>
  );
};

export default HorarioList;
