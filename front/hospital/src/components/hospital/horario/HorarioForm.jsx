import * as HorarioServer from "./HorarioServer";

import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

import {listMedicos} from "../medico/MedicoServer"
import { registerHorario } from "./HorarioServer";

const HorarioForm = () => {
    let navigate = useNavigate();
    const params = useParams();
    console.log(params);
    const initialState = {
      activo: true,
      fecha_atencion: "",
      inicio_atencion: "",
      final_atencion: "",
      usuarioregistro: 1,
      usuariomodificacion: 1,
      medico_id: ""
    };
    const[medicos, setMedicos]= useState([])
    const listMedico = async () => {
      try {
        const res2 = await listMedicos(medicos);
        const data2 = await res2.json();
        console.log(data2);
        setMedicos(data2);
      } catch (error) {
        console.log(error);
      }
    };
    
    function handleoOnchage(e) {
      const { name, value } = e.target;
      setHorarios({ ...horarios, [name]: value });
    }

    const handleSubmit = (e) => {
      e.preventDefault();
      try {
        if (!params.id) {
          registerHorario(horarios)
            .then((rest) => {
              //console.log(rest);
            })
            .catch((err) => {
              console.log(err);
            });
        } else {
          HorarioServer.updateHorario(params.id, horarios);
        }

        navigate("/listhorario", { replace: true });
      } catch (error) {
        console.log(error);
      }
    };

    const getHorario = async (horarioId) => {
      try {
        const result = await HorarioServer.geteHorario(
          horarioId
        );
        const data = await result.json();
        const {
          fecha_atencion,
          inicio_atencion,
          final_atencion,
          usuarioregistro,
          usuariomodificacion,
          medico_id,
        } = data;
        setHorarios({
          fecha_atencion,
          inicio_atencion,
          final_atencion,
          usuarioregistro,
          usuariomodificacion,
          medico_id,
        });
      } catch (error) {
        console.log(error);
      }
    };

    useEffect(() => {
      listMedico();
      if (params.id) {
        getHorario(params.id);
      }
    }, []);

    const [horarios, setHorarios] = useState(initialState);
    return (
      <div className="container">
        <h1 className="mb-4 text-center mt-3">Formulario Horarios</h1>
        <form onSubmit={handleSubmit}>
          <div className="mb-3">
            <label htmlFor="exampleInputEmail1" className="form-label">
              Fecha de atencion
            </label>
            <input
              type="date"
              className="form-control"
              name="fecha_atencion"
              value={horarios.fecha_atencion}
              onChange={handleoOnchage}
            />
          </div>
          <div className="mb-3">
            <label htmlFor="exampleInputPassword1" className="form-label">
              Inicio Atencion
            </label>
            <input
              type="time"
              className="form-control"
              name="inicio_atencion"
              value={horarios.inicio_atencion}
              onChange={handleoOnchage}
            />
          </div>
          <div className="mb-3">
            <label htmlFor="exampleInputPassword1" className="form-label">
              Final atencion
            </label>
            <input
              type="time"
              className="form-control"
              name="final_atencion"
              value={horarios.final_atencion}
              onChange={handleoOnchage}
            />
          </div>

          {/*  <div className="mb-3">
            <label htmlFor="exampleInputPassword1" className="form-label">
              Medico
            </label>
            <input
              type="number"
              className="form-control"
              //name="medico_id"
              value={horarios.medico_id}
              onChange={handleoOnchage}
            />
          </div> */}
          <div className="mb-3">
            <label htmlFor="exampleInputPassword1" className="form-label">
              Usuario que regustra
            </label>
            <input
              type="text"
              className="form-control"
              name="usuariomodificacion"
              value={horarios.usuarioregistro}
              onChange={handleoOnchage}
            />
          </div>

          <div>
            <label htmlFor="exampleInputPassword1" className="form-label">
              Medico
            </label>
            <select
              className="form-select"
              aria-label="Default select example"
              name="medico_id"
              onChange={handleoOnchage}
            >
              {medicos.map((medico) => {
                let m = medico.id;
                return (
                  <option value={m} key={medico.id}>
                    {medico.apellidos}
                  </option>
                );
              })}
            </select>
          </div>

          <div className="mb-3">
            <label htmlFor="exampleInputPassword1" className="form-label">
              Usuario que modifica
            </label>
            <input
              type="text"
              className="form-control"
              name="usuariomodificacion"
              value={horarios.usuariomodificacion}
              onChange={handleoOnchage}
            />
          </div>
          <div>
            {params.id ? (
              <button type="submit" className="btn btn-dark">
                Editar
              </button>
            ) : (
              <button type="submit" className="btn btn-primary">
                Submit
              </button>
            )}
          </div>
        </form>
      </div>
    );
};

export default HorarioForm