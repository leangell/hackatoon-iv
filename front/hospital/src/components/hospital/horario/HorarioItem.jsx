import React from "react";
import { deleteHorario } from "./HorarioServer";
import { useNavigate } from "react-router-dom";

const HorarioItem = ({ horario }) => {
  const navigate = useNavigate();
  const handleDelete = (horarioId) => {
    deleteHorario(horarioId);
    //navigate("/", { replace: true });
  };

  return (
    <div className="col-md-4">
      <div className="card mb-3">
        <h5 className="card-header">{horario.fecha_atencion}</h5>
        <div className="card-body">
          <h5 className="card-title">Inicio atencion</h5>
          <p className="card-text">{horario.inicio_atencion}</p>
          <h5 className="card-title">Final atencion</h5>
          <p className="card-text">{horario.final_atencion}</p>
          <h5 className="card-title">Medico</h5>
          <p className="card-text">{horario.medico_id}</p>
          <div className="row">
            <div className="col-3">
              <button
                onClick={() => navigate(`/updateHorario/${horario.id}`)}
                className="btn btn-dark mx-2"
              >
                Update
              </button>
            </div>
            <div className="col-3">
              <form>
                <button
                  type="submit"
                  onClick={() => horario.id && handleDelete(horario.id)}
                  className="btn btn-danger mx-2"
                >
                  Delete
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HorarioItem;
