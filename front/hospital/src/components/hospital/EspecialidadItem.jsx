import React from "react"
import { deleteSpecialidades } from "./EspecialidadesServer";
import { useNavigate } from "react-router-dom";

const EspecialidadItem= ({especialidad}) => {
    const navigate = useNavigate();
    const handleDelete=(especialidadId)=>{
      deleteSpecialidades(especialidadId); 
      //navigate("/", { replace: true });
    }

   
    return (
      <div className="col-md-4">
        <div className="card mb-3">
          <h5 className="card-header">{especialidad.nombre}</h5>
          <div className="card-body">
            <h5 className="card-title">Descripcion</h5>
            <p className="card-text">{especialidad.descripcion}</p>
            {/* <a href="#" className="btn btn-primary">
              Go somewhere
            </a> */}
            <div className="row">
              <div className="col-3">
                <button
                  onClick={() => navigate(`/updateEspecialidad/${especialidad.id}`)}
                  className="btn btn-dark mx-2"
                >
                  Update
                </button>
              </div>
              <div className="col-3">
                <form>
                  <button
                    type="submit"
                    onClick={() =>
                      especialidad.id && handleDelete(especialidad.id)
                    }
                    className="btn btn-danger mx-2"
                  >
                    Delete
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
};

export default EspecialidadItem