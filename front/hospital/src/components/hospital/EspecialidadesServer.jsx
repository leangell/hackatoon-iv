import axios from "axios";
const API_URL = "http://127.0.0.1:8000/especialidad/";

export const listSpecialidades = async () => {
    return await fetch(API_URL);
};

export const geteEspecialidad = async (id) => {
  return await fetch(`${API_URL}${id}`);
};

export const registerSpecialidades = async (especialidad) => {
    return await axios.post(API_URL, especialidad, {
        headers: {
            "Content-Type": "application/json",
        },
    });
}

export const updateEspecialidad = async (id, especialidad) => {
  return await axios.put(`${API_URL}${id}/`, especialidad, {
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export const deleteSpecialidades = async (id) => {
  return await axios.delete(`${API_URL}${id}`);
};

