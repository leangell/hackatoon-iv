import * as EspecialidadesServer from './EspecialidadesServer'

import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

import {registerSpecialidades} from './EspecialidadesServer'

const EspecialidadForm = ()=>{
  let navigate = useNavigate();
  const params = useParams()
  console.log(params)
  const initialState = {nombre: "",
                        descripcion: "", 
                        usuarioregistro: 1, 
                        usuariomodificacion:1,
                        activo: 'true',
                      }
  
  function handleoOnchage(e) {
    const{ name,value}= e.target;
    setEspecialidad({...especialidad, [name]:value});
  }
  
  const handleSubmit = (e) =>{
    e.preventDefault();
    try {
      if(!params.id){
        registerSpecialidades(especialidad)
          .then((rest) => {
            console.log(rest);
          })
          .catch((err) => {
            console.log(err);
          });
      }else{
        EspecialidadesServer.updateEspecialidad(params.id, especialidad)
      }
      
      navigate("/", { replace: true });
    } catch (error) {
      console.log(error)
    }
  }
  
  const getEspecialidad = async (especialidadId) => {
      try {
        const result = await EspecialidadesServer.geteEspecialidad(
          especialidadId
        );
        const data = await result.json();
        const{nombre, descripcion, usuarioregistro, usuariomodificacion} = data;
        setEspecialidad({
          nombre,
          descripcion,
          usuarioregistro,
          usuariomodificacion
      });
      } catch (error) {
        console.log(error);
      }
  };

  useEffect(() => {
    if(params.id){
      getEspecialidad(params.id)
    }
  },[])
   
  
  const [especialidad, setEspecialidad] = useState(initialState)
    return (
      <div className="container">
        <h1 className="mb-4 text-center mt-3">Formulario especialidades</h1>
        <form onSubmit={handleSubmit}>
          <div className="mb-3">
            <label htmlFor="exampleInputEmail1" className="form-label">
              Nombre
            </label>
            <input
              type="text"
              className="form-control"
              name="nombre"
              value={especialidad.nombre}
              onChange={handleoOnchage}
            />
          </div>
          <div className="mb-3">
            <label htmlFor="exampleInputPassword1" className="form-label">
              Descripcion
            </label>
            <input
              type="text"
              className="form-control"
              name="descripcion"
              value={especialidad.descripcion}
              onChange={handleoOnchage}
            />
          </div>
          <div className="mb-3">
            <label htmlFor="exampleInputPassword1" className="form-label">
              usuario registro
            </label>
            <input
              type="text"
              className="form-control"
              name="usuarioregistro"
              value={especialidad.usuarioregistro}
              onChange={handleoOnchage}
            />
          </div>

          <div className="mb-3">
            <label htmlFor="exampleInputPassword1" className="form-label">
              usuario que modifica
            </label>
            <input
              type="text"
              className="form-control"
              name="usuariomodificacion"
              value={especialidad.usuariomodificacion}
              onChange={handleoOnchage}
            />
          </div>
          <div>
            {params.id ? (
              <button type="submit" className="btn btn-dark">
                Editar
              </button>
            ) : (
              <button type="submit" className="btn btn-primary">
                Submit
              </button>
            )}
          </div>
        </form>
      </div>
    );
};

export default EspecialidadForm